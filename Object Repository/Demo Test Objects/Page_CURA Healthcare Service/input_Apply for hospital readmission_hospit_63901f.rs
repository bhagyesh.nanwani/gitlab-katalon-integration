<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Apply for hospital readmission_hospit_63901f</name>
   <tag></tag>
   <elementGuidId>289d6b91-cf4f-4368-badf-0f34b0c83d05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk_hospotal_readmission']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#chk_hospotal_readmission</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>cdc665e0-33af-4124-93bc-13e99c9d0ac2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>c0f97816-fedb-4809-bf64-efbe79314304</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>b52b8d1d-e994-469e-b9f1-fdd58f060d6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>1255805b-44c7-4b15-a566-36ea910c1359</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>72af9fd1-9a1e-489c-8cd6-1a50b92b454a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk_hospotal_readmission&quot;)</value>
      <webElementGuid>a41e06df-67b4-440b-80e7-a05118fc3651</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk_hospotal_readmission']</value>
      <webElementGuid>6377fd9b-ec66-4831-980d-46e521686230</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label/input</value>
      <webElementGuid>244145a1-151d-4f81-9c95-039d28ff4c9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>42fa691f-9cea-404f-aa7c-84a947a1e98f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk_hospotal_readmission' and @name = 'hospital_readmission']</value>
      <webElementGuid>527cb4d2-3369-43aa-863e-6b863b6fed26</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
